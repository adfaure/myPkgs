{ pkgs }:

pkgs.stdenv.mkDerivation rec {
  batsim_src = pkgs.fetchgit {
        url = "https://github.com/oar-team/batsim";
        rev = "94dedf0a222e01c6317355f00d5a01add649e9bb";
        sha256 = "1wninixi9wazlpscjgfvhx8pclfcy3fznf16f62fdij6ygb3xzry";
  };

  name = "batsim_exp_tools";
  src = batsim_src;
  patches = [ ./patch_swf_to_batsim_workload_delay ];
  buildPhase = ''
    # Patch tests script she bang
    # patchShebangs ..
    # Patch inside scripts in the yaml test files that are not catch by
    # patchShebangs utility
    # find .. -type f | xargs sed -i -e 's#\(.*\)/usr/bin/env\(.*\)#\1${pkgs.coreutils}/bin/env\2#'
  '';

  installPhase=''
    mkdir -p $out/bin
    cp -R tools/* $out/bin
    cp tools/experiments/execute_one_instance.py $out/bin
    cp tools/experiments/execute_instances.py $out/bin
    '';
}
