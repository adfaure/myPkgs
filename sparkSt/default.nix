{ pkgs }:
pkgs.spark.overrideDerivation(attrs: rec {
  buildInputs = attrs.buildInputs ++ [ pkgs.procps ];
  # Todo this is not very clear what we really need to do.
  # In the install phase, we reconfigure spark.
  installPhase =  attrs.installPhase + ''
    cat > $out/lib/${attrs.untarDir}/conf/spark-env.sh <<- EOF
    export SPARK_LOG_DIR="/home/afaure/scratch/spark/logs"
    export SPARK_WORKER_DIR="/home/afaure/scratch/spark/work"
    EOF

    cat > $out/lib/${attrs.untarDir}/conf/spark-defaults.conf <<- EOF
      spark.eventLog.enabled           true
      spark.eventLog.dir               /home/afaure/scratch/spark-events/
      spark.local.dir                  /home/afaure/scratch/
      spark.executor.memory            4g
    EOF

    # Create symlink to have simpler acces
    ln -s $out/lib/${attrs.untarDir} $out/lib/spark
    '';
})

