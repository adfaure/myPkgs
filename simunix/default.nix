{ stdenv, fetchFromGitHub, cmake, simgrid_remotesg, remote_simgrid, boost, cppzmq, zeromq, thrift, glibcLocales, sqlite }:

stdenv.mkDerivation rec {
  name = "simunix-${version}";
  version = "0.1.0";

  src = ./simunix;

  nativeBuildInputs = [
    #sqlite
    remote_simgrid
    simgrid_remotesg
    boost
    cppzmq
    zeromq
    cmake
    thrift
    sqlite
  ];

  buildInputs = [ glibcLocales ];

  cmakeFlags = ["-DCMAKE_BUILD_TYPE=Debug"
                "-Denable_warnings=ON"
                "-Dtreat_warnings_as_errors=OFF"];

  enableParallelBuilding = true;

  meta = with stdenv.lib; {
    description = "A solution to execute your distributed application on top of SimGrid.";
    homepage    = "https://github.com/simgrid/remote-simgrid";
    platforms   = platforms.unix;
  };
}
