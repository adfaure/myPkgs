{ pkgs ? import <nixpkgs> {} }:

  pkgs.rWrapper.override {
    packages = with pkgs.rPackages; [
      pkgs.pandoc
      pkgs.R
      fmsb
      devtools
      GGally
      ggplot2
      reshape2
      yaml
      optparse
      knitr
      rmarkdown
      dplyr
      plyr
      viridis
      stringi
      gtools
      rgl
      pkgs.pandoc
      pkgs.coreutils
      pkgs.which
      corrplot
      corrr
      readr
      tidyverse
      reshape
      reshape2
      dplyr
      ggplot2
      reshape2
    ];
}

