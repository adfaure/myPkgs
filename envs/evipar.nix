{ pkgs, myPkgs, dmnix, mkShell, lublin99, fetchgit }:
  let
  # but I it will solve a problem that
  # I have with spark by making the robin
  # wrapper accessible.
  # Plus the repository is big for nothing...
  run_robin = pkgs.stdenv.mkDerivation {
    name = "parallevi";

    src = fetchgit {
      url = "https://gitlab.inria.fr/adfaure/evipar/";
      sha256 = "0pps9asy2fnkyhhnn284jay3k8vfgi7lr4kz4b0h75m1pphfv56v";
      rev = "debbb56529715da3122d4376e4ae3d6c2b412bea";
    };

    installPhase=''
      mkdir -p $out/bin
      cp scripts/run_robin.sh $out/bin
    '';

  };

  RwithDeps = pkgs.rWrapper.override {
    packages = with pkgs.rPackages; [
      tidyverse
      viridis
      reshape
      lubridate
    ];
  };

  pydeps =
   (pkgs.python3.withPackages (ps: with ps; [
      dmnix.humanfriendly
      dmnix.coloredlogs
      dmnix.execo
      dmnix.evalys
#      (async-timeout.overrideAttrs (attr:  rec {
#        version = "1.2.0";
#        pname = "async-timeout";
#        name = "${pname}-${version}";
#       src = pkgs.fetchurl  {
#            url = "mirror://pypi/${builtins.substring 0 1 pname}/${pname}/${name}.tar.gz";
#            sha256 = "06bsq7x525csikszhvrlfv0b0ffhpnk577yrjjnfashmq04d2lh6";
#          };
#        }
#      ))
      colorlog
      pandas
      pyyaml
      docopt
      sortedcontainers
      colored ]));

    deps = [
      pkgs.openjdk
      # Workload generation model
      lublin99
      # We need spark
      pkgs.procps
      myPkgs.sparkSt
      # Scala for spark jobs
      pkgs.scala_2_11
      # sbt to compile scala
      pkgs.sbt
      # Our scheduler
      myPkgs.polys

      # Get the batsim tool
      myPkgs.batsim_exp_tools
      # My scripts
      myPkgs.myScripts
      # Robin
      dmnix.batexpe
      dmnix.batsim140
      run_robin

      pydeps

      RwithDeps
    ];

in

pkgs.buildEnv rec {

  name = "rnv";
  paths = deps;

}
