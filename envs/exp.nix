{ pkgs, myPkgs, dmnix }:
pkgs.buildEnv {
  name = "expEnv";
  paths = [
    #FIXME I don't understand why, but
    # I need to use python35 to have evalys
    # installed. Moreover, if I need this
    # full block to have python installed.
    # I don't understand why.
    (pkgs.python.withPackages (ps: with ps; [
      dmnix.humanfriendly
      dmnix.coloredlogs
      dmnix.execo
      dmnix.evalys
     # (async-timeout.overrideAttrs (attr:  rec {
     #   version = "1.2.0";
     #   pname = "async-timeout";
     #   name = "${pname}-${version}";
     #  src = pkgs.fetchurl  {
     #       url = "mirror://pypi/${builtins.substring 0 1 pname}/${pname}/${name}.tar.gz";
     #       sha256 = "06bsq7x525csikszhvrlfv0b0ffhpnk577yrjjnfashmq04d2lh6";
     #     };
     #   }
     # ))
      colorlog
      pandas
      pyyaml
      docopt
      sortedcontainers
      colored
    ]))
  ];
}
