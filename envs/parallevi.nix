{ pkgs, myPkgs, dmnix, mkShell, lublin99 }:
  let
  # but I it will solve a problem that
  # I have with spark by making the robin
  # wrapper accessible.
  # Plus the repository is big for nothing...
  run_robin = pkgs.stdenv.mkDerivation {
    name = "parallevi";
    src = pkgs.fetchgit {
      url = "https://gitlab.inria.fr/adfaure/parallevi";
      rev = "7572ffd55bf8d8763feffc27c878a2060cad661d";
      sha256 = "1kj7p5hx3wbjjbzkqj4dnlc71600d23jhxscp589h72h4qap7zag";
    };

    installPhase=''
      mkdir -p $out/bin
      cp experiments/scripts/run_robin.sh $out/bin
    '';
  };

  RwithDeps = pkgs.rWrapper.override {
    packages = with pkgs.rPackages; [
      tidyverse
      viridis
      reshape
      lubridate
    ];
  };

  pydeps =
   (pkgs.python35.withPackages (ps: with ps; [
      dmnix.humanfriendly
      dmnix.coloredlogs
      dmnix.execo
      dmnix.evalys
      (async-timeout.overrideAttrs (attr:  rec {
        version = "1.2.0";
        pname = "async-timeout";
        name = "${pname}-${version}";
       src = pkgs.fetchurl  {
            url = "mirror://pypi/${builtins.substring 0 1 pname}/${pname}/${name}.tar.gz";
            sha256 = "06bsq7x525csikszhvrlfv0b0ffhpnk577yrjjnfashmq04d2lh6";
          };
        }
      ))
      colorlog
      pandas
      pyyaml
      docopt
      sortedcontainers
      colored
    ]));

    deps = [
      # Workload generation model
      lublin99
      # We need spark
      pkgs.procps
      myPkgs.sparkG5K
      # Scala for spark jobs
      pkgs.scala_2_11
      # sbt to compile scala
      pkgs.sbt

      # Our scheduler
      myPkgs.polys

      # Get the batsim tool
      myPkgs.batsim_exp_tools
      # My scripts
      myPkgs.myScripts
      # Robin
      dmnix.batexpe
      dmnix.batsim140
      run_robin

      pydeps

      RwithDeps
    ];

in

pkgs.buildEnv rec {

  name = "rnv";
  paths = deps;

}
