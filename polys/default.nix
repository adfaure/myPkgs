{ stdenv, fetchgit , rustPlatform, makeWrapper, zeromq, pkgconfig}:

with rustPlatform;

buildRustPackage rec {
  name = "polys-${version}";
  version = "1.2.1";

  propagatedBuildInputs = [
    pkgconfig
    zeromq
  ];

  src = fetchTarball "https://gitlab.inria.fr/adfaure/polysched/-/archive/4cb9d13ac8d960d7f3afd7e1f0ed10dfa21c8062/polysched-4cb9d13ac8d960d7f3afd7e1f0ed10dfa21c8062.tar.gz";

#  src = fetchgit {
#      url = "https://gitlab.inria.fr/adfaure/polysched";
#      rev = "80ebeb144c8ef5837f2528cb308b583c49532d30";
#      sha256 = "06137lddzv07khc82mak62rc32vwbkkg31g1w1y8ibnqkvx27vq7";
#  };

  depsSha256 = "0220xkppabxyq2n7csimis59bnykzip1xv2k74yp4n3452zms6d8";
  cargoSha256 = "03lsxhn92ivrw73g6i9gwk2qyvvyfckcglzcyq1pfdjmgqrhypz6";
}
