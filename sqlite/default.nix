{ stdenv, fetchurl, perl }:

stdenv.mkDerivation rec {
  name = "sqlite-${version}";
  version = "3.24.0";

  src = fetchurl {
    url = "https://www.sqlite.org/2018/sqlite-autoconf-3240000.tar.gz";
    sha256 = "0jmprv2vpggzhy7ma4ynmv1jzn3pfiwzkld0kkg6hvgvqs44xlfr";
  };

  buildInputs = [ perl ];

  meta = with stdenv.lib; {
    description = "Commandline tools for searching and browsing sourcecode";
    homepage    = http://uzix.org/cgvg.html;
    license     = licenses.gpl2;
    platforms   = platforms.unix;
  };
}
