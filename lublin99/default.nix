{ pkgs, stdenv, gcc, fetchgit, ninja }:

stdenv.mkDerivation rec {
  name = "lublin99-${version}";
  version = "0.1.0";

  src = fetchTarball "https://gitlab.inria.fr/adfaure/lublin99/-/archive/master/lublin99-master.tar.gz";

  nativeBuildInputs = [
    gcc
    ninja
  ];

  buildPhase = "ninja";

  installPhase =  ''
    mkdir -p $out/bin
    cp m_lublin99 $out/bin
  '';

  meta = with stdenv.lib; {
    description = "Parameterized version of the lublin model available on
    parallel workload archive";
    platforms   = platforms.unix;
  };
}
