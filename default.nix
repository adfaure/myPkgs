{
  pkgs ? import (
  # For the moment, in order to have the package polys working, I use this version of nix.
  fetchTarball "https://github.com/NixOS/nixpkgs-channels/archive/e860b651d6e297658e960c165fd231dbc0de1f9b.tar.gz"
  # fetchTarball "https://github.com/NixOS/nixpkgs/archive/17.09.tar.gz"
  ) {},
  dmnix ? import (
    fetchTarball
    "https://gitlab.inria.fr/vreis/datamove-nix/repository/73c1243e5c7be995f2db7f27131b8c167d1faa45/archive.tar.gz"
  ) { inherit pkgs;},
}:

let
  callPackage = pkgs.lib.callPackageWith (pkgs // pkgs.xlibs // self // dmnix) ;
  self = rec {

    inherit pkgs dmnix;

    # Install with: nix-env -f "<nixpkgs>" -iA renv -f ~/Projects/myPkgs
    rEnv = callPackage ./envs/rEnv.nix { };
    paralleviEnv = callPackage ./envs/parallevi.nix {
      inherit pkgs dmnix;
      myPkgs = self;
    };

    paralleviShell = pkgs.mkShell rec {
      buildInputs = [ paralleviEnv ];
    };

    eviparEnv = callPackage ./envs/evipar.nix {
      inherit pkgs dmnix;
      myPkgs = self;
    };

    eviparShell = pkgs.mkShell rec {
      buildInputs = [ eviparEnv ];
    };


    expEnv = callPackage ./envs/exp.nix {
      inherit pkgs dmnix;
      myPkgs = self;
    };

    polysdevShell = pkgs.mkShell rec {
      buildInputs = [ pkgs.cargo ];
    };

    # Shells
    # Activate it using: nix-shell -A batrustenv ~/Projects/myPkgs
    batrustenv = callPackage ./shells/batrust.nix { myPkgs = self; inherit pkgs; };

    batsim_exp_tools = callPackage ./batsim_exp_tools { inherit pkgs; };
    evalysEnv = dmnix.evalysEnv;
    sqlite = callPackage ./sqlite {};
    simunix = callPackage ./simunix { inherit sqlite; };

    lublin99 = callPackage ./lublin99 {};

    polys = callPackage ./polys {};
    myScripts = callPackage ./scripts {};

    sparkSt = callPackage ./sparkSt { inherit pkgs; };
    sparkG5K = callPackage ./sparkSt/g5k.nix { inherit pkgs; };
  };
in
  self

