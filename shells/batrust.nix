{ myPkgs, pkgs } :
  pkgs.stdenv.mkDerivation rec {
    name = "rustEnv";
    propagatedBuildInputs = [
      myPkgs.pkgs.sl
      myPkgs.pkgs.zeromq
      myPkgs.pkgs.cargo
      myPkgs.pkgs.pkgconfig
      myPkgs.pkgs.coreutils
      myPkgs.pkgs.rustfmt
      myPkgs.pkgs.psmisc
      myPkgs.pkgs.coreutils
      myPkgs.pkgs.commonsCompress

      myPkgs.batsim_exp_tools

      myPkgs.dmnix.batexpe
      myPkgs.dmnix.batsim.buildInputs
      myPkgs.dmnix.batsim.nativeBuildInputs
      myPkgs.dmnix.batsim140
    ];
}
