{ pkgs, fetchgit }:
pkgs.stdenv.mkDerivation {

  name = "myScripts";

  src = fetchgit {
    url = "https://gitlab.inria.fr/adfaure/scripts";
    rev = "dd9c01989ed0d4e83b9948da913e0f428ce1fa9e";
    sha256 = "0myi2xpa79jdzw90r91c672q1n1cl6f5v176773najh5v8yvl7pl";
  };

  installPhase=''
    mkdir -p $out/bin
    cp ./* $out/bin
    ls -l $out/bin
  '';

}

